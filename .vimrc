let mapleader=','
set encoding=utf-8
set backupdir=~/.vim/
set directory=~/.vim/

set nocompatible
set number
set hlsearch
set noerrorbells
set ignorecase smartcase
set showcmd
set shiftwidth=4
set expandtab
set tabstop=4
set laststatus=2
set cursorline
hi CursorLine cterm=none
hi MatchParen ctermfg=blue ctermbg=none

filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
    Plugin 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    Plugin 'VundleVim/Vundle.vim'
    Plugin 'scrooloose/nerdtree'
    Plugin 'kien/ctrlp.vim'
    Plugin 'bling/vim-airline'
call vundle#end()
filetype plugin indent on

let g:deoplete#enable_at_startup = 1


let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/](\.git|node_modules|vendor)$',
    \ 'file': '\v\.(exe|so|dll|zip|tar|tar.gz|pyc)$',
    \ }


set viminfo='100,<500
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif


map <C-e> :NERDTreeToggle<CR>
map <leader>r :NERDTreeFind<cr>
let NERDTreeIgnore=['\.pyc$', '__pycache__', 'venv', 'node_modules']
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


if has('nvim')
    nmap <BS> <C-h>
    hi Search ctermfg=black
    au FileType html setlocal tabstop=2 shiftwidth=2
    au FileType javascript setlocal tabstop=2 shiftwidth=2
    au FileType css setlocal tabstop=2 shiftwidth=2
endif

inoremap <c-s> <ESC>:update<CR>
nnoremap <silent> <C-S> :<C-u>update<CR>
" command! -nargs=+ Gr !grep --color -Inir --exclude-dir={node_modules,dist,.git} --exclude={package-lock.json,*.min.js,*.map} <args>
" nmap <c-h> gT
" nmap <C-l> gt

au BufNewFile,BufRead *.vue set filetype=vue
au BufReadPost *.vue set filetype=html
au BufReadPost *.styl set syntax=css
set clipboard+=unnamedplus
autocmd BufEnter * :syntax sync fromstart
