alias ..='cd ..'
alias ...='cd ..;cd ..'
alias ls='ls --color=auto'
alias ll='ls --color=auto -hl'
alias diff='diff --color'
alias grep='grep --color -ni'

alias gst='git status'
alias gc='git checkout'
alias gd='git diff'
alias gb='git branch -vv'
alias gp='git push'
alias gl='git pull'
alias lp='git pull && git push'
alias gw='git show'
alias gam='git commit -am '
alias gr='git remote -v'

alias vi='nvim'

alias sw='source ~/.bin/splitWin.sh'

# export PATH="/opt/homebrew/opt/node@16/bin:$PATH"
# export NPM_TOKEN=glpat-xyumrcLmcKMSVHVA82HK
export PATH="/opt/homebrew/opt/node@20/bin:$PATH"

# brew
export HOMEBREW_API_DOMAIN="https://mirrors.tuna.tsinghua.edu.cn/homebrew-bottles/api"
export HOMEBREW_BOTTLE_DOMAIN="https://mirrors.tuna.tsinghua.edu.cn/homebrew-bottles"
export HOMEBREW_BREW_GIT_REMOTE="https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/brew.git"
export HOMEBREW_CORE_GIT_REMOTE="https://mirrors.tuna.tsinghua.edu.cn/git/homebrew/homebrew-core.git"
export HOMEBREW_PIP_INDEX_URL="https://pypi.tuna.tsinghua.edu.cn/simple"

# Created by `pipx` on 2024-03-23 03:30:10
export PATH="$PATH:/Users/xschen/.local/bin"
