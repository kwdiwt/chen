echo '(1) centos (2) arch'
read release

if [ $release = 1 ]; then
    sudo yum install -y vim git npm
else
    sudo pacman -S --noconfirm vim git npm
fi

git submodule init
git submodule update

sudo ln -s $HOME/.vim /root/.vim
sudo ln -s $HOME/.vimrc /root/.vimrc
