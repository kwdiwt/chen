call plug#begin('~/.vim/plugged')
    Plug 'scrooloose/nerdtree'
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'vim-airline/vim-airline'

    " Plug 'neovim/nvim-lspconfig'
    " Plug 'prabirshrestha/vim-lsp'
    " Plug 'mattn/vim-lsp-settings'
    " Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    " Plug 'kyazdani42/nvim-web-devicons' " for file icons
    " Plug 'kyazdani42/nvim-tree.lua'
call plug#end()

let mapleader=','

" deoplete
let g:deoplete#enable_at_startup = 1

let g:lsp_settings = {
\  'typescript-language-server': {
\    'disabled': 1,
\   }
\}

" airline
let g:airline_section_z = airline#section#create(['linenr', 'maxlinenr'])
let g:airline#extensions#default#layout = [['a', 'b', 'c'], ['x', 'z']]
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#tabline#show_tab_nr = 0
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#show_close_button = 0

" ctrlp
let g:ctrlp_open_new_file = 't'
let g:ctrlp_show_hidden = 1
let g:ctrlp_working_path_mode = 'a'
let g:ctrlp_open_multiple_files = 't'
let g:ctrlp_custom_ignore = {
\  'dir':  '\v[\/](\.git|\.hg|\.svn|\.umi|\.idea|node_modules|vendor|dist)$',
\  'file': '\v\.(exe|so|dll|png|jpg|jpeg|svg|gif|webp)$',
\  'link': 'some_bad_symbolic_links',
\}

" nerdtree
map <C-e> :NERDTreeToggle<CR>
map <leader>r :NERDTreeFind<cr>
nmap <c-h> gT
nnoremap <C-l> gt
let NERDTreeQuitOnOpen=1
let NERDTreeDirArrowCollapsible=''
let NERDTreeDirArrowExpandable=''
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\.git$', '\.pyc$', '__pycache__', 'venv', 'node_modules']
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

set number
set expandtab
set ignorecase smartcase
set tabstop=2
set shiftwidth=2
set clipboard+=unnamedplus
set foldmethod=indent
set foldlevelstart=99
set scrolloff=10
colorscheme zellner
highlight Search ctermbg=3 ctermfg=black
highlight Visual ctermbg=black ctermfg=yellow
highlight MatchParen ctermbg=DarkGray ctermfg=white
inoremap <c-s> <ESC>:update<CR>
nnoremap <silent> <C-S> :<C-u>update<CR>
" format json
" nnoremap <F6> :%!python -m json.tool<CR>

" fu! SaveSess()
"     execute 'mksession! ' . getcwd() . '/.session.vim'
" endfunction
" 
" fu! RestoreSess()
" if filereadable(getcwd() . '/.session.vim')
"     execute 'so ' . getcwd() . '/.session.vim'
"     if bufexists(1)
"         for l in range(1, bufnr('$'))
"             if bufwinnr(l) == -1
"                 exec 'sbuffer ' . l
"             endif
"         endfor
"     endif
" endif
" endfunction
" 
" autocmd VimLeave * call SaveSess()
" autocmd VimEnter * nested call RestoreSess()

" let prefix='~/.config/nvim/session/%:p:h:t.vim'
" fu! SaveSess()
"   let name='Session.vim'
"   execute 'mksession! '.prefix
" endfunction
" autocmd VimLeave * call SaveSess()
" 
" fu! RestoreSess()
"   if len(argv()) > 0
"     return
"   endif
"   execute 'mksession! '.prefix
"   " execute 'call mkdir(~/.config/nvim/session/sb.vim)'
" 
"   " let name='Session.vim'
"   " let session=''.name
"   " if filereadable(expand(session))
"   "   execute 'source '.session
"   " endif
" endfunction
" autocmd VimEnter * nested call RestoreSess()


highlight SignColumn ctermbg=none
highlight CursorColumn ctermfg=7 ctermbg=242
highlight PmenuSel ctermbg=0 ctermfg=white
highlight Folded ctermfg=4 ctermbg=none guifg=DarkBlue guibg=LightGrey
" highlight Pmenu ctermfg=255 ctermbg=Black
" highlight CocHighlightText ctermfg=black ctermbg=3

" lua << EOF
" local nvim_lsp = require('lspconfig')
" local on_attach = function(client, bufnr)
"   local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
"   local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
" 
"   buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
" 
"   -- Mappings.
"   local opts = { noremap=true, silent=true }
"   buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
"   buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
"   buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
"   buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
"   buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
"   buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
"   buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
"   buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
"   buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
"   buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
"   buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
"   buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
"   buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
"   buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
"   buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
" 
"   -- Set some keybinds conditional on server capabilities
"   if client.resolved_capabilities.document_formatting then
"     buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
"   elseif client.resolved_capabilities.document_range_formatting then
"     buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
"   end
" 
"   -- Set autocommands conditional on server_capabilities
"   if client.resolved_capabilities.document_highlight then
"     vim.api.nvim_exec([[
"       hi LspReferenceRead cterm=bold ctermbg=red guibg=LightYellow
"       hi LspReferenceText cterm=bold ctermbg=red guibg=LightYellow
"       hi LspReferenceWrite cterm=bold ctermbg=red guibg=LightYellow
"       augroup lsp_document_highlight
"         autocmd!
"         autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
"         autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
"       augroup END
"     ]], false)
"   end
" end
" 
" -- Use a loop to conveniently both setup defined servers
" -- and map buffer local keybindings when the language server attaches
" local servers = {  }
" for _, lsp in ipairs(servers) do
"   nvim_lsp[lsp].setup { on_attach = on_attach }
" end
" EOF
